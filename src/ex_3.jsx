import React, { Component } from "react";
import { Text, TextInput, View } from "react-native";

export default class AddMeUp extends Component {
  constructor(props) {
    super(props);
    this.state = { primary: "0", secondary: "0", result: "0" };
    this._takePri = this._takePri.bind(this);
    this._takeSec = this._takeSec.bind(this);
  }

  _takePri = (data) => {
    if (data === "") this.setState({ primary: "0" });
    else this.setState({ primary: data });
    this.calculate(data, this.state.secondary);
  };
  _takeSec = (data) => {
    if (data === "") this.setState({ secondary: "0" });
    else this.setState({ secondary: data });
    this.calculate(data, this.state.primary);
  };

  calculate = (a, b) => {
    console.log("A:" + a + "B:" + b);
    if (!a && b) this.setState({ result: b });
    else if (a && !b) this.setState({ result: a });
    else if (!a && !b) this.setState({ result: "0" });
    else this.setState({ result: parseFloat(a) + parseFloat(b) });
  };
  render() {
    return (
      <View style={{ padding: 10, marginTop: 100 }}>
        <TextInput
          keyboardType="phone-pad"
          style={{
            marginTop: 20,
            padding: 5,
            height: 40,
            backgroundColor: "steelblue",
            color: "white",
            fontSize: 20
          }}
          placeholder="Enter a number"
          onChangeText={this._takePri}
        />
        <Text style={{ marginTop: 20 }}>+</Text>
        <TextInput
          keyboardType="phone-pad"
          style={{
            marginTop: 20,
            padding: 5,
            height: 40,
            backgroundColor: "steelblue",
            color: "white",
            fontSize: 20
          }}
          placeholder="Enter a number"
          onChangeText={this._takeSec}
        />
        <Text style={{ marginTop: 20, fontSize: 100 }}>{this.state.result}</Text>
      </View>
    );
  }
}
