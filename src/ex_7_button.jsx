import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";

const propButtonStyles = StyleSheet.create({
    buttonFont: {
      color: "#000",
      fontSize: 30,
      alignSelf: "center"
    },
    button: {
      width: "90%",
      flexDirection: "row",
      borderWidth: 2,
      borderColor: "#ffffff",
      borderRadius: 5,
      paddingTop: 5,
      paddingBottom: 5,
      backgroundColor: "rgba(255,255,255,0.4)",
      margin: 10
    }
  });

export default function PropButton (props) {
    return (
        <TouchableOpacity style={propButtonStyles.button} onPress={props.handler}>
              <View style={{
                  width: 20,
                  height: 20,
                  borderRadius: 100 / 2,
                  borderWidth: 2,
                  borderColor: "#ffffff",
                  backgroundColor: props.color,
                  margin: 20
              }}></View>
          <Text style={propButtonStyles.buttonFont}>{props.color}</Text>
        </TouchableOpacity>
      );
  }