import React, { Component } from "react";
import { StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity } from "react-native";

export default class WeatherApp extends Component {
  constructor(props) {
    super(props);
    this.state = { isLoading: true, selectedIndex: 0 };
  }
  APIkey = "387a0f18af5012093e0cd20e3d06031e";

  kelvinToCel = (data) => {
    return (data - 273.15).toFixed(2);
  };

  bgFromId = (data) => {
    if (data < 300) return "https://i.ibb.co/PTb9PQ8/Thunderstorm.jpg";
    else if (data < 500) return "https://i.ibb.co/x3jmYcN/Drizzle.jpg";
    else if (data < 600) return "https://i.ibb.co/CH1y9p0/Rain.jpg";
    else if (data < 700) return "https://i.ibb.co/gdcpcmN/Snow.jpg";
    else if (data < 800) return "https://i.ibb.co/t3nRBhP/Fog.jpg";
    else if (data === 800) return "https://i.ibb.co/yYSq1YC/Clear.jpg";
    else return "https://i.ibb.co/1RpC0yX/Cloudy.jpg";
  };

  updateSelectedIndex = (data) => {
    this.setState({ selectedIndex: data });
  };

  parseResult = (data) => {
    var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var items = {
      city: data.city.name,
      weatherDay: []
    };
    var catcher = null;
    var temp = {
      day: null,
      dt: null,
      temp: null,
      minTemp: null,
      maxTemp: null,
      category: null,
      details: null,
      iconUrl: null,
      bgUrl: null
    };
    for (var x of data.list) {
      if (catcher === null || catcher === this.parseDate(x.dt_txt).getDay()) {
        if (temp.day === null) temp.day = this.parseDate(x.dt_txt).getDay();
        if (temp.dt === null) temp.dt = this.parseDate(x.dt_txt).toString();
        if (temp.temp === null) temp.temp = this.kelvinToCel(x.main.temp);
        if (temp.minTemp === null || temp.minTemp > x.main.temp_min)
          temp.minTemp = this.kelvinToCel(x.main.temp_min);
        if (temp.maxTemp === null || temp.maxTemp < x.main.temp_max)
          temp.maxTemp = this.kelvinToCel(x.main.temp_max);
        if (temp.category === null) temp.category = x.weather[0].id;
        if (temp.details === null) temp.details = x.weather[0].description;
        if (temp.iconUrl === null)
          temp.iconUrl = "http://openweathermap.org/img/wn/" + x.weather[0].icon + "@2x.png";
        if (temp.bgUrl === null) temp.bgUrl = this.bgFromId((temp.category = x.weather[0].id));

        if (catcher === null) catcher = this.parseDate(x.dt_txt).getDay();
      } else if (catcher !== this.parseDate(x.dt_txt).getDay()) {
        temp.day = dayNames[temp.day];
        items.weatherDay.push(JSON.parse(JSON.stringify(temp)));

        temp.day = this.parseDate(x.dt_txt).getDay();
        temp.dt = this.parseDate(x.dt_txt).toString();
        temp.temp = this.kelvinToCel(x.main.temp);
        temp.minTemp = this.kelvinToCel(x.main.temp_min);
        temp.maxTemp = this.kelvinToCel(x.main.temp_max);
        temp.category = x.weather[0].id;
        temp.details = x.weather[0].description;
        temp.iconUrl = "http://openweathermap.org/img/wn/" + x.weather[0].icon + "@2x.png";
        temp.bgUrl = this.bgFromId((temp.category = x.weather[0].id));
        catcher = temp.day;
      }
    }
    console.log("API Data Loaded " + new Date());
    return items;
  };

  parseDate = (date) => {
    var dt = new Date(date.split(" ")[0]);
    return dt;
  };

  //talisay 1683881
    //cebu  1717512
  componentDidMount = () => {
    return fetch("https://api.openweathermap.org/data/2.5/forecast?id=1683881&appid=" + this.APIkey)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState(
          {
            isLoading: false,
            dataSource: this.parseResult(responseJson)
          },
          function() {}
        );
      })
      .catch((error) => {
        console.error(error);
      });
  };

  render() {
    if (this.state.isLoading) {
      return <View style={{ flex: 1, padding: 20 }}></View>;
    } else
      return (
        <View style={styles.container}>
          <ImageBackground
            source={{ uri: this.state.dataSource.weatherDay[this.state.selectedIndex].bgUrl }}
            style={styles.header}
          >
            <TextItem text={this.state.dataSource.city} size={30} />
            <Image
              style={{ width: 100, height: 100, alignSelf: "center", margin: 0 }}
              source={{ uri: this.state.dataSource.weatherDay[this.state.selectedIndex].iconUrl }}
            />
            <TextItem
              text={this.state.dataSource.weatherDay[this.state.selectedIndex].details}
              size={32}
            />
            <TextItem
              text={this.state.dataSource.weatherDay[this.state.selectedIndex].temp + "°"}
              size={30}
            />
          </ImageBackground>
          <View style={styles.futureDays}>
            {this.state.dataSource.weatherDay.map((name, index) => {
              return (
                <TouchableOpacity
                  style={styles.button}
                  key={index.toString()}
                  onPress={() => this.updateSelectedIndex(index)}
                >
                  <TextItem text={this.state.dataSource.weatherDay[index].day} size={24} />
                  <Image
                    style={{ width: 75, height: 75, marginTop: 0 }}
                    source={{ uri: this.state.dataSource.weatherDay[index].iconUrl }}
                  />
                  <TextItem text={this.state.dataSource.weatherDay[index].maxTemp} size={24} />
                  <TextItem text={this.state.dataSource.weatherDay[index].minTemp} size={24} />
                </TouchableOpacity>
              );
            })}
          </View>
        </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#333",
    paddingBottom: 50
  },
  sectionCol: {
    flex: 1,
    flexDirection: "column"
  },
  header: {
    flex: 4,
    flexDirection: "column",
    paddingVertical: 50
  },
  sectionRow: { flex: 1, flexDirection: "row", alignContent: "space-between" },
  button: {
    flex: 1,
    flexDirection: "row",
    alignContent: "space-between",
    borderWidth: 1,
    borderColor: "#333"
  },
  futureDays: {
    flex: 6,
    flexDirection: "column"
  }
});

class TextItem extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <Text
          style={{
            fontSize: this.props.size,
            color: "#fff"
          }}
        >
          {this.props.text}
        </Text>
      </View>
    );
  }
}
