import React, { Component } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";

export default class Calculator extends Component {
    
styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: "column"
    },
    column: {
      flex: 1,
      flexDirection: "column",
      backgroundColor: "#914679"
    },
    row: {
      flex: 3,
      flexDirection: "row",
      backgroundColor: "#1C095E"
    },
    rowControl: {
      flex: 3,
      flexDirection: "row",
      backgroundColor: "#6697A7"
    },
    button: {
      flex: 1,
      justifyContent: "center",
      borderWidth: 0.5,
      borderColor: "#d6d7da"
    },
    buttonFont: {
      color: "#62FAC2",
      fontSize: 30,
      fontWeight: "bold",
      alignSelf: "center"
    },
    buffer: {
      flex: 1,
      backgroundColor: "rgb(155, 155, 255)",
      padding: 20,
      flexDirection: "row",
      justifyContent: "flex-end"
    },
    result: {
      flex: 1,
      backgroundColor: "rgb(255, 155, 255)",
      padding: 20,
      flexDirection: "row",
      justifyContent: "flex-end"
    },
    controls: {
      flex: 5,
      flexDirection: "row",
      backgroundColor: "rgb(255, 155, 155)"
    },
    controlsSub3: {
      flex: 3
    },
    buttonText: {
      flex: 5,
      backgroundColor: "rgb(255, 155, 155)"
    }
  });

    constructor(props) {
    super(props);
    this.state = { buffer: "", result: 0, decimalUse: false };
  }

  _onPressButton = (data) => {
    console.log(this.state.buffer);
    {
      /*Reenable the decimal if an operator is pressed*/
    }
    if (this.isOperator(data)) this.disableDecimal(false);
    if (data === "=") this.evaluate(this.state.buffer);
    else if (data === "AC") {
      {
        /*Clear all buffer and results*/
      }
      this.setState({ buffer: "", result: 0, decimalUse: false });
      return;
    } else if (data === "DEL") {
      {
        /*Clear delete one character from buffer*/
      }
      {
        /*if the character to be deleted is a decimal, delete it and enable the decimal button*/
      }
      if (this.state.buffer[this.state.buffer.length - 1] === ".") {
        {
          /*if the character to be deleted is a decimal, delete it and enable the decimal button*/
        }
        this.disableDecimal(false);
        this.setState({ buffer: this.state.buffer.substring(0, this.state.buffer.length - 1) });
      }
      if (this.isOperator(this.state.buffer[this.state.buffer.length - 1])) {
        {
          /*if the character to be deleted is an operator, check for preceding decimals prior to the latest operator, then disable the decimal button as needed.*/
        }
        i = 2;
        while (i) {
          console.log(this.state.buffer[this.state.buffer.length - i]);
          if (this.isOperator(this.state.buffer[this.state.buffer.length - i])) {
            this.setState({ buffer: this.state.buffer.substring(0, this.state.buffer.length - 1) });
            return;
          }
          if (this.state.buffer[this.state.buffer.length - i] === ".") {
            this.disableDecimal(true);
            console.log("I found a decimal, gonna disable the button!");
          }
          if (this.state.buffer.length - i === 0) {
            this.setState({ buffer: this.state.buffer.substring(0, this.state.buffer.length - 1) });
            console.log("I reached the end of the expression!");
            return;
          }
          i++;
        }
      }
      this.setState({ buffer: this.state.buffer.substring(0, this.state.buffer.length - 1) });
      return;
    } else if (this.isOperator(this.state.buffer[this.state.buffer.length - 1]) && data === ".") {
      {
        /*append a 0 if decimal is pressed immediately after an operator, then disable the decimal button*/
      }
      this.setState({ buffer: this.state.buffer.concat("0" + data) });
      this.disableDecimal(true);
    } else if (
      (this.state.buffer === "0" && data !== "0" && !this.isOperator(data)) ||
      (this.state.buffer[this.state.buffer.length - 1] === "0" &&
        this.isOperator(this.state.buffer[this.state.buffer.length - 2]))
    ) {
      {
        /*Overwrite starting 0 with nonzero integer.*/
      }
      if (data === ".") {
        this.setState({ buffer: this.state.buffer.concat(data) });
        this.disableDecimal(true);
        return;
      }
      this.setState({
        buffer: this.state.buffer.substring(0, this.state.buffer.length - 1).concat(data)
      });
    } else if (this.state.buffer === "" && (this.isOperator(data) || data === ".")) {
      {
        /*Disallow operator input if no integers are in play (also includes '.')*/
      }
      return;
    } else if (
      (!isNaN(this.state.buffer[this.state.buffer.length - 1]) || !this.state.buffer === "") &&
      data === "."
    ) {
      {
        /*Allow a decimal point to be used if preceding character is an integer and then disable the button*/
      }
      this.setState({ buffer: this.state.buffer.concat(data) });
      this.disableDecimal(true);
    } else if (
      (this.isOperator(this.state.buffer[this.state.buffer.length - 1]) ||
        this.state.buffer[this.state.buffer.length - 1] === ".") &&
      this.isOperator(data)
    ) {
      {
        /*Overwrite last entered operators or trailing decimal*/
      }
      if (this.state.buffer[this.state.buffer.length - 1] === ".") this.disableDecimal(false);
      this.setState({
        buffer: this.state.buffer.substring(0, this.state.buffer.length - 1).concat(data)
      });
    } else this.setState({ buffer: this.state.buffer.concat(data) });
  };

  evaluate(data) {
    if (isNaN(data[data.length - 1])) {
      data = data.substring(0, this.state.buffer.length - 1);
      this._onPressButton("DEL");
    }
    this.setState({ result: eval(data) });
  }

  disableDecimal(data) {
    this.setState({ decimalUse: data });
  }

  isOperator(data) {
    return data === "/" || data === "*" || data === "-" || data === "+";
  }

  render() {
    return (
      <View style={this.styles.container}>
        <View style={this.styles.buffer}>
          <Text style={{ color: "#1C095E", fontWeight: "bold", fontSize: 30 }}>
            {this.state.buffer}
          </Text>
        </View>
        <View style={this.styles.result}>
          <Text style={{ color: "#1C095E", fontWeight: "bold", fontSize: 30 }}>
            {this.state.result}
          </Text>
        </View>
        <View style={this.styles.controls}>
          <View style={this.styles.controlsSub3}>
            {/*Row of buttons*/}
            <View style={this.styles.rowControl}>
              <TouchableOpacity style={this.styles.button} onPress={() => this._onPressButton("AC")}>
                <Text style={this.styles.buttonFont}>AC</Text>
              </TouchableOpacity>
              <TouchableOpacity style={this.styles.button} onPress={() => this._onPressButton("DEL")}>
                <Text style={this.styles.buttonFont}>DEL</Text>
              </TouchableOpacity>
            </View>
            {/*Row of buttons*/}
            <View style={this.styles.row}>
              <TouchableOpacity style={this.styles.button} onPress={() => this._onPressButton("7")}>
                <Text style={this.styles.buttonFont}>7</Text>
              </TouchableOpacity>
              <TouchableOpacity style={this.styles.button} onPress={() => this._onPressButton("8")}>
                <Text style={this.styles.buttonFont}>8</Text>
              </TouchableOpacity>
              <TouchableOpacity style={this.styles.button} onPress={() => this._onPressButton("9")}>
                <Text style={this.styles.buttonFont}>9</Text>
              </TouchableOpacity>
            </View>
            {/*Row of buttons*/}
            <View style={this.styles.row}>
              <TouchableOpacity style={this.styles.button} onPress={() => this._onPressButton("4")}>
                <Text style={this.styles.buttonFont}>4</Text>
              </TouchableOpacity>
              <TouchableOpacity style={this.styles.button} onPress={() => this._onPressButton("5")}>
                <Text style={this.styles.buttonFont}>5</Text>
              </TouchableOpacity>
              <TouchableOpacity style={this.styles.button} onPress={() => this._onPressButton("6")}>
                <Text style={this.styles.buttonFont}>6</Text>
              </TouchableOpacity>
            </View>
            {/*Row of buttons*/}
            <View style={this.styles.row}>
              <TouchableOpacity style={this.styles.button} onPress={() => this._onPressButton("1")}>
                <Text style={this.styles.buttonFont}>1</Text>
              </TouchableOpacity>
              <TouchableOpacity style={this.styles.button} onPress={() => this._onPressButton("2")}>
                <Text style={this.styles.buttonFont}>2</Text>
              </TouchableOpacity>
              <TouchableOpacity style={this.styles.button} onPress={() => this._onPressButton("3")}>
                <Text style={this.styles.buttonFont}>3</Text>
              </TouchableOpacity>
            </View>
            {/*Row of buttons*/}
            <View style={this.styles.row}>
              <TouchableOpacity
                style={this.styles.button}
                onPress={() => this._onPressButton(".")}
                disabled={this.state.decimalUse}
              >
                <Text style={this.styles.buttonFont}>.</Text>
              </TouchableOpacity>
              <TouchableOpacity style={this.styles.button} onPress={() => this._onPressButton("0")}>
                <Text style={this.styles.buttonFont}>0</Text>
              </TouchableOpacity>
              <TouchableOpacity style={this.styles.button} onPress={() => this._onPressButton("=")}>
                <Text style={this.styles.buttonFont}>=</Text>
              </TouchableOpacity>
            </View>
          </View>
          {/*Column of buttons*/}
          <View style={this.styles.column}>
            <TouchableOpacity style={this.styles.button} onPress={() => this._onPressButton("/")}>
              <View>
                <Text style={this.styles.buttonFont}>/</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={this.styles.button} onPress={() => this._onPressButton("*")}>
              <View>
                <Text style={this.styles.buttonFont}>*</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={this.styles.button} onPress={() => this._onPressButton("-")}>
              <View>
                <Text style={this.styles.buttonFont}>-</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={this.styles.button} onPress={() => this._onPressButton("+")}>
              <View>
                <Text style={this.styles.buttonFont}>+</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

