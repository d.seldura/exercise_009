import React from "react";
import { StyleSheet, Text, View } from "react-native";

export default function HelloWorld() {
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <Text style={styles.fontColor}>Hello There!</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  content: {
    backgroundColor: "steelblue",
    width: 150,
    height: 150,
    alignItems: "center",
    justifyContent: "center"
  },
  fontColor: {
    fontWeight: "bold",
    color: "white",
    fontSize: 30,
    textAlignVertical: "center"
  }
});
