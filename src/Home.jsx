import React from 'react';
import { View, StyleSheet } from 'react-native';
import Custom_Button from './Custom_Button';

export default function Home({ navigation }) {
  const NavItems = ['Ex_1 HelloWorld','Ex_2 Button Basics','Ex_3 Add Me Up','Ex_4 Luminous','Ex_5 TextStyler','Ex_6 Calculator','Ex_7 Color Switcher','Ex_8_Prelim Weather'];
  const buttonStyle = StyleSheet.create({
    button: {
      alignSelf: 'center',
      borderWidth: 3,
      borderColor: '#ffffff',
      borderRadius: 45,
      backgroundColor: '#6f2d91',
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 6,
      },
      shadowOpacity: 0.39,
      shadowRadius: 8.3,
      elevation: 13,
      width: '80%',
          height: 45,
          padding: 6,
      marginVertical: 5,
    },
    buttonFont: {
      color: '#62FAC2',
      fontSize: 20,
      alignSelf: 'center',
    },
  });
  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      {NavItems.map((name, index) => {
        return (
          <Custom_Button
            key={index}
            label={name}
            styles={buttonStyle}
            onPress={() =>navigation.navigate({ name })}
          />
        );
      })}
    </View>
  );
}
