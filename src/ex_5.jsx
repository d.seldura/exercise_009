import React, { Component } from "react";
import { Text, TextInput, View, StyleSheet, CheckBox } from "react-native";
import { RadioButton } from 'react-native-paper';

export default class TextStyler extends Component {

    layout = StyleSheet.create({
        container: {
          flex: 1,
          backgroundColor: '#fff'
        }
      });
      
  constructor(props) {
    super(props);
    this.state = { text: "Sample Text", bold: false, italic: false, weight: "normal", style: "normal", color:"red" };
    this._textHandler = this._textHandler.bind(this);
    this._boldCheck = this._boldCheck.bind(this);
    this._italicCheck = this._italicCheck.bind(this);
  }

  _boldCheck = () =>{
    this.setState({bold: !this.state.bold, weight: !this.state.bold ? "bold" : "normal"});
  }

  _italicCheck = () =>{
    this.setState({italic: !this.state.italic, style: !this.state.italic ? "italic" : "normal"});
  }

  _textHandler = (data) => {
    this.setState({ text: data });
  }

  render() {
    const { color } = this.state;
    
    return (
    <View style={this.layout.container}>
    <TextInput
      keyboardType="default"
      style={{
        marginTop: 30, 
        padding: 10, 
        height: 40, 
        borderBottomWidth: 1,
        borderBottomColor: 'gray',
        fontSize: 20
      }}
      onChangeText={this._textHandler}
      value={this.state.text}
    />
    <View 
      style={{
        flexDirection: "row",
        alignItems: "baseline",
        marginBottom: 5

      }}>
    <CheckBox value={this.state.bold} onValueChange={this._boldCheck}/>
    <Text style={{fontSize: 20}}>
    Bold</Text>
    
    <CheckBox value={this.state.italic} onValueChange={this._italicCheck}/>
    <Text style={{fontSize: 20}}>Italic</Text>
    </View>
       
    <View>
      <View style={{  flexDirection: "row",  alignItems: "baseline" }}>
        <RadioButton
          value="red"
          status={color === 'red' ? 'checked' : 'unchecked'}
          onPress={() => { this.setState({ color: 'red' }); }}
        />  
        <Text style={{fontSize: 20}}>Red</Text>
      </View>
      
      <View style={{  flexDirection: "row",  alignItems: "baseline" }}>
        <RadioButton
          value="green"
          status={color === 'green' ? 'checked' : 'unchecked'}
          onPress={() => { this.setState({ color: 'green' }); }}
        />
        <Text style={{fontSize: 20}}>Green</Text>
        </View>
        
      <View style={{  flexDirection: "row",  alignItems: "baseline" }}>
        <RadioButton
          value="blue"
          status={color === 'blue' ? 'checked' : 'unchecked'}
          onPress={() => { this.setState({ color: 'blue' }); }}
        />
        <Text style={{fontSize: 20}}>Blue</Text>
        </View>
      </View>
    <Text
      style={{
        marginTop: 30, 
        padding: 10, 
        fontSize: 50,
        fontStyle: this.state.style,
        fontWeight: this.state.weight,
        color: this.state.color
      }}
      >{this.state.text}</Text>
    </View>

  );
}
}