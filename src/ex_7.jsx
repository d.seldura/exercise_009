import React, { useState } from "react";
import { View } from "react-native";
import PropButton from './ex_7_button';


export default function ColorSwitcher() {
  const colors = ["red", "yellow", "blue", "green", "rgb(255,120,1)", "salmon"];
  const [color, setColor] = useState("red");
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          backgroundColor: color,
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        {colors.map((name, index) => {
          return <PropButton key={index} color={name} handler={() => setColor(name)} />;
        })}
      </View>
    );
}
