import React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './src/Home';
import HelloWorld from './src/ex_1';
import ButtonBasics from './src/ex_2';
import AddMeUp from './src/ex_3';
import Luminous from './src/ex_4';
import TextStyler from './src/ex_5';
import Calculator from './src/ex_6';
import ColorSwitcher from './src/ex_7';
import WeatherApp from './src/ex_8_prelim';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Ex_1 HelloWorld" component={HelloWorld} />
      <Stack.Screen name="Ex_2 Button Basics" component={ButtonBasics} />
      <Stack.Screen name="Ex_3 Add Me Up" component={AddMeUp} />
      <Stack.Screen name="Ex_4 Luminous" component={Luminous} />
      <Stack.Screen name="Ex_5 TextStyler" component={TextStyler} />
      <Stack.Screen name="Ex_6 Calculator" component={Calculator} />
      <Stack.Screen name="Ex_7 Color Switcher" component={ColorSwitcher} />
      <Stack.Screen name="Ex_8_Prelim Weather" component={WeatherApp} />
    </Stack.Navigator>
  </NavigationContainer>
  );
}